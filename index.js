const https = require('http');
//Mock Database
const database = [
{
"firstName": "Mary Jane",
"lastName": "Dela Cruz",
"mobileNo": "09123456789",
"email": "mjdelacruz@mail.com",
"password": 123
},
{
"firstName": "John",
"lastName": "Doe",
"mobileNo": "09123456789",
"email": "jdoe@mail.com",
"password": 123
}
]
//client to server .parse and server to client stringify
https.createServer(function(req, res){
	if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {'Content-Type':'application/json'});
		res.write(JSON.stringify(database));
		res.end();
	}if(req.url == "/register" && req.method == "POST"){
		let requestBody = '';
		req.on('data', function(data){
			requestBody += data
		});
		req.on('end', function(){
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody);

		let newUser = {
			"firstName": requestBody.firstName, 
			"lastName": requestBody.lastName,
			"mobileNo": requestBody.mobileNo,
			"email": requestBody.email,
			"password": requestBody.password
		}
		// Add the new user into the mock database
		database.push(newUser);
		console.log(database);
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(newUser));
		res.end()
		
	})
	}
}).listen(4000);
console.log('CRUD server running at localhost:4000');